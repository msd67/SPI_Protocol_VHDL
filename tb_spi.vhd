LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
 
ENTITY tb_spi IS
END tb_spi;
 
ARCHITECTURE behavior OF tb_spi IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT spi
    PORT(
         clock : IN  std_logic;
         send : IN  std_logic;							-- Master Test Bench
         busy : OUT  std_logic;
         Data_Ready : OUT  std_logic;
         Send_Data : IN  unsigned(7 downto 0);		-- Master Test Bench
         Receive_Data : OUT  unsigned(7 downto 0);
         SS_Device : IN  unsigned(0 downto 0);		-- Master Test Bench
         SCLK : OUT  std_logic;
         MOSI : OUT  std_logic;
         MISO : IN  std_logic;
         SS : OUT  unsigned(0 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clock : std_logic := '0';
   signal send : std_logic := '0';
   signal Send_Data : unsigned(7 downto 0) := (others => '0');
   signal SS_Device : unsigned(0 downto 0) := (others => '0');
   signal MISO : std_logic := '0';

 	--Outputs
   signal busy : std_logic;
   signal Data_Ready : std_logic;
   signal Receive_Data : unsigned(7 downto 0);
   signal SCLK : std_logic;
   signal MOSI : std_logic;
   signal SS : unsigned(0 downto 0);

   -- Clock period definitions
   constant clock_period : time := 10 ns;
   constant SCLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: spi PORT MAP (
          clock => clock,
          send => send,
          busy => busy,
          Data_Ready => Data_Ready,
          Send_Data => Send_Data,
          Receive_Data => Receive_Data,
          SS_Device => SS_Device,
          SCLK => SCLK,
          MOSI => MOSI,
          MISO => MISO,
          SS => SS
        );

   -- Clock process definitions
   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
   end process;
 
   SCLK_process :process
   begin
		SCLK <= '0';
		wait for SCLK_period/2;
		SCLK <= '1';
		wait for SCLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clock_period*10;

      -- insert stimulus here 
		
--		ss_device			<=		"1";
--		send_data			<=		"00110110";
--		send					<=		'1';
--		wait for clock_period;
--		send					<=		'0';

		ss_device			<=		"1";
		send_data			<=		"10110110";
		send					<=		'1';
		wait for clock_period;
		send					<=		'0';
		
		wait for clock_period*10;
		MISO					<=		'1';			--	DATA IN = 10010110
		wait for clock_period;
		MISO					<=		'0';
		wait for clock_period;
		MISO					<=		'0';
		wait for clock_period;
		MISO					<=		'1';
		wait for clock_period;
		MISO					<=		'0';
		wait for clock_period;
		MISO					<=		'1';
		wait for clock_period;
		MISO					<=		'1';
		wait for clock_period;
		MISO					<=		'0';
		
      wait;
   end process;

END;
