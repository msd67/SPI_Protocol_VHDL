library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity spi is
	Generic (
		NOD		:	INTEGER	:=	1;									--	Number Of Device
		DW			:	INTEGER	:=	8									--	Data Width
																			-- The serial mode supports data rates of up to 80 Mbps.
																			-- This serial data rate is 50 Mbps.
	);
    Port ( clock 			: in  	STD_LOGIC;
           send 			: in  	STD_LOGIC;
           busy 			: out  	STD_LOGIC;
           Data_Ready 	: out  	STD_LOGIC;
           Send_Data 	: in  	unsigned (7 downto 0);
           Receive_Data : out  	unsigned (7 downto 0);
			  SS_Device		: in		unsigned	(0 downto 0);	--	allows more than one device on the same serial communications line
           SCLK 			: out  	STD_LOGIC;
           MOSI 			: out  	STD_LOGIC;					-- Master Mode, In Slave Mode use instead of MISO
           MISO			: in  	STD_LOGIC;					-- Master Mode, In Slave Mode use instead of MOSI
           SS				: out  	unsigned (0 downto 0));	--	Slave Select
end spi;

architecture Behavioral of spi is
	signal	clk				:		STD_LOGIC	:=	'0';
	signal	send_int			:		STD_LOGIC	:=	'0';
	signal	send_prev		:		STD_LOGIC	:=	'0';
	signal	busy_int			:		STD_LOGIC	:=	'0';
	signal	Data_Ready_int	:		STD_LOGIC	:=	'0';
	signal	DORD				:		STD_LOGIC	:=	'0';		--	Data Order
	signal	MSTR				:		STD_LOGIC	:=	'0';		--	Master/Slave Select
	signal	CPOL				:		STD_LOGIC	:=	'0';		--	Clock Polarity
	signal	SCLK_LOCK		:		STD_LOGIC	:=	'0';
	
	signal	Send_Data_int		:		unsigned	(DW-1 downto 0)	:=	(others	=>	'0');
	signal	Shift_out			:		unsigned	(DW-1 downto 0)	:=	(others	=>	'0');		-- Shift out from this register
	signal	Receive_Data_int	:		unsigned	(DW-1 downto 0)	:=	(others	=>	'0');		--	Shift in from this register
	signal	SS_int				:		unsigned	(NOD-1 downto 0)	:=	(others	=>	'0');		-- Slave Select
	signal	me						:		unsigned	(NOD-1 downto 0)	:=	(others	=>	'0');		-- This Slave Address
	signal	count					:		unsigned	(3 downto 0)		:=	(others	=>	'0');
	
	Type		State_Type			is		(Idle, Addressing, InSending, InReceiving);
	signal	State_Reg			:		State_Type;
	
	component DCM
	port
	 (-- Clock in ports
	  CLK_IN1           : in     std_logic;
	  -- Clock out ports
	  CLK_OUT1          : out    std_logic
	 );
	end component;

begin
	System_clock : DCM
	  port map
		(-- Clock in ports
		 CLK_IN1 => clock,
		 -- Clock out ports
		 CLK_OUT1 => clk);
		 
	process (clk)
	begin
		if	rising_edge(clk)	then
			send_int				<=		send;
			send_prev			<=		send_int;
			busy					<=		busy_int;
			Data_Ready			<=		Data_Ready_int;
			Data_Ready_int		<=		'0';
			Send_Data_int		<=		Send_Data;
			Receive_Data		<=		Receive_Data_int;
			SS_int				<=		SS_Device;
			SCLK_LOCK			<=		'0';
			
			case	State_Reg	is
				when	Idle			=>
					if	((send_int='1') and (send_prev='0') and (MSTR='0'))	then		-- This just for Master mode
						busy_int			<=		'1';
						Shift_out		<=		Send_Data_int;
						State_Reg		<=		Addressing;
					end if;
					if	((ss_int=me) and (MSTR='1'))	then										-- This just for Slave mode
						State_Reg		<=		InReceiving;
					end if;
				when	Addressing	=>
					SS					<=		not SS_int;												--	CS is an active low input.
					State_Reg		<=		InSending;
				when	InSending	=>
					SCLK_LOCK			<=			'1';
					count					<=			count	+ 1;
					if	(DORD='0')	then																--	MSB	First
						MOSI				<=			Shift_out(to_integer(DW-count-1));
					end if;
					if	(DORD='1')	then																-- LSB	First
						MOSI				<=			Shift_out(to_integer(count));
					end if;
					if	(count=to_unsigned(DW-1, 4) and Shift_out(DW-1)='0')	then		--	Data was Send
						State_Reg		<=			Idle;
						busy_int			<=			'0';
						SS					<=			(others	=>	'1');
						count				<=			(others	=>	'0');
					end if;
					if	(count=to_unsigned(DW-1, 4) and Shift_out(DW-1)='1')	then		--	Instruction was Send
						State_Reg		<=			InReceiving;
						count				<=			(others	=>	'0');
					end if;
				when	InReceiving	=>																	--	Master Request Data from slave Or Slave is Getting Data
					SCLK_LOCK			<=			'1';
					count												<=			count	+ 1;
					Receive_Data_int(to_integer(count))		<=			MISO;
					if	DORD='0'	then
						Receive_Data_int(to_integer(DW-count-1))		<=			MISO;					
					end if;
					if	(count=to_unsigned(DW-1, 4))	then
						State_Reg		<=			Idle;
						busy_int			<=			'0';
						Data_Ready_int	<=			'1';
						SS					<=			(others	=>	'1');
						count				<=			(others	=>	'0');
					end if;
			end case;
					
				if	(Data_Ready_int='1' and Receive_Data_int(DW-1)='1' and MSTR='1')	then	--	In Slave mode
					Shift_out			<=			"01110111";		-- Requested Data from Master
					State_Reg			<=			InSending;
				end if;
		end if;
	end process;
	
	SCLK			<=		CPOL	when	SCLK_LOCK='0'	else	clk;									--	SCLK frequency	=	50Mhz


end Behavioral;

